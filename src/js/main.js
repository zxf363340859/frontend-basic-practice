function fetchData(url) {
  return fetch(url)
    .then(result => result.json())
    .catch(error => error);
}

const url = 'http://localhost:3000/person';

fetchData(url)
  .then(result => {
    $('#info-name').html(result.name);
    $('#info-age').html(result.age);
    $('#info-desc').html(result.description);
    const eduArr = result.educations;
    for (let i = 0; i < eduArr.length; i++) {
      $('#edu').append(`
        <li>
                <div>
                    <span class="edu-year"><strong>${eduArr[i].year}</strong></span>
                    <div class="edu-desc">
                        <strong>${eduArr[i].title}</strong> <br/>
                        ${eduArr[i].description}
                    </div>
                </div>
            </li>`);
    }
  })
  .catch(error => {
    document.writeln(error);
  });
